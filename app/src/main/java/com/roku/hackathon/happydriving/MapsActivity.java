package com.roku.hackathon.happydriving;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.util.FloatMath;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import dao.NotificationDao;
import listener.map.HappyDrivingLocationListener;
import server.api.HappyDrivingApi;
import service.AlertService;

public class MapsActivity extends FragmentActivity implements SensorListener {
    public static int LOCATION_REFRESH_TIME = 5000;
    public static int LOCATION_REFRESH_DISTANCE = 0;
    private GoogleMap mMap;
    private LocationManager  mLocationManager;
    HappyDrivingLocationListener mLocationListener;
    SensorManager mSensorManager;
    BroadcastReceiver receiver;
    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorManager.registerListener(this,
                SensorManager.SENSOR_ACCELEROMETER,
                SensorManager.SENSOR_DELAY_GAME);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showMarker();
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        serviceIntent = new Intent(getApplicationContext(),
                AlertService.class);
        startService(serviceIntent);

        registerReceiver(receiver, new IntentFilter(
                AlertService.BROADCAST_ACTION));
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                initLocationManager();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void initLocationManager(){
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        boolean gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!gps_enabled && !network_enabled) {
            Toast.makeText(this, "Please enable GPS!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        String provider = LocationManager.GPS_PROVIDER;
        if (network_enabled)
            provider = LocationManager.NETWORK_PROVIDER;
        mLocationListener = new HappyDrivingLocationListener(mMap, this, mLocationManager);
        mLocationManager.requestLocationUpdates(provider, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, mLocationListener);

        //go to current location
        Location location = mLocationManager.getLastKnownLocation(provider);
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        }
        showMarker();
    }

    private Location getCurrentLocation() {
        boolean gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!gps_enabled && !network_enabled) {
            Toast.makeText(this, "Please enable GPS!", Toast.LENGTH_SHORT).show();
            finish();
            return null;
        }
        String provider = LocationManager.GPS_PROVIDER;
        if (network_enabled)
            provider = LocationManager.NETWORK_PROVIDER;

        return mLocationManager.getLastKnownLocation(provider);
    }

    private void showMarker() {
        if (NotificationDao.LIST == null)
            return;
        mMap.clear();
        for(NotificationDao item : NotificationDao.LIST) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(item.getLat(), item.getLng())));
        }
    }

    private static final float SHAKE_THRESHOLD_GRAVITY = 2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 500;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;
    private long mShakeTimestamp;
    private int mShakeCount;
    @Override
    public void onSensorChanged(int sensor, float[] values) {
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float gX = x / SensorManager.GRAVITY_EARTH;
        float gY = y / SensorManager.GRAVITY_EARTH;
        float gZ = z / SensorManager.GRAVITY_EARTH;

        // gForce will be close to 1 when there is no movement.
        float gForce = FloatMath.sqrt(gX * gX + gY * gY + gZ * gZ);

        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                return;
            }

            // reset the shake count after 3 seconds of no shakes
            if (mShakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
                mShakeCount = 0;
            }

            mShakeTimestamp = now;
            mShakeCount++;

            if (mShakeCount >= 2){
                mShakeCount = 0;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Location location = getCurrentLocation();
                        final double lat = location.getLatitude();
                        final double lng = location.getLongitude();
                        String url = HappyDrivingApi.Notification_URL + "?"
                                + "location[lat]=" + lat
                                + "&location[lng]=" + lng;
                        (new HappyDrivingApi()).makeServiceCall(url, HappyDrivingApi.POST);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(lat, lng)));
                            }
                        });
                    }
                }).start();
            }
        }
    }

    @Override
    public void onAccuracyChanged(int sensor, int accuracy) {

    }

    @Override
    protected void onPause() {
        super.onPause();
//        stopService(serviceIntent);
        unregisterReceiver(receiver);
    }
}
