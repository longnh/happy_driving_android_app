package com.roku.hackathon.happydriving;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.roku.hackathon.happydriving.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.util.List;

import dao.NotificationDao;
import server.api.HappyDrivingApi;

public class FlashScreenActivity extends Activity {

    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flash_screen);
        (new FetchTask(this)).execute();
    }

    class FetchTask extends AsyncTask<Void, Void, Void> {

        private Context ctx;

        public FetchTask(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String a = (new HappyDrivingApi())
                        .makeServiceCall(HappyDrivingApi.Notification_URL, HappyDrivingApi.GET);
                Type listType = new TypeToken<List<NotificationDao>>() {}.getType();
                Gson gson = new Gson();
                NotificationDao.LIST = (List<NotificationDao>) gson.fromJson(a, listType);
            } catch (Exception ex) {
                Toast.makeText(ctx, "Fail to fetch data!", Toast.LENGTH_SHORT).show();
            } finally {
                Intent i = new Intent(ctx, MapsActivity.class);
                startActivity(i);
                ((Activity) ctx).finish();
            }
            return null;
        }
    }

}
