package service;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.roku.hackathon.happydriving.MapsActivity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import dao.NotificationDao;
import listener.map.HappyDrivingLocationListener;
import server.api.HappyDrivingApi;

public class AlertService extends Service {

    public static boolean isRunning  = false;
    public static float ALERT_RANGE = 100;

    private LocationManager mLocationManager;

    BroadcastReceiver broadcaster;
    Intent intent;

    static final public String BROADCAST_ACTION = "com.pavan.broadcast";
    public AlertService(){}

    public AlertService(LocationManager lm) {
        mLocationManager = lm;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);
        isRunning = true;

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        boolean gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        String provider = LocationManager.GPS_PROVIDER;
        if (network_enabled)
            provider = LocationManager.NETWORK_PROVIDER;
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(provider, MapsActivity.LOCATION_REFRESH_TIME,
                MapsActivity.LOCATION_REFRESH_DISTANCE, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });
    }

    @Override
    public void onDestroy() {
        isRunning = false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRunning) {
                    Log.d("AlertService", "Runing");
                    String a = (new HappyDrivingApi())
                            .makeServiceCall(HappyDrivingApi.Notification_URL, HappyDrivingApi.GET);
                    Type listType = new TypeToken<List<NotificationDao>>() {}.getType();
                    Gson gson = new Gson();
                    List<NotificationDao> list = (List<NotificationDao>) gson.fromJson(a, listType);
                    if(list!=null)
                        NotificationDao.LIST = list;
                    sendResult();
                    alert(getCurrentLocation());
                    Log.d("AlertService", a);
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return Service.START_STICKY;
    }

    private Location getCurrentLocation(){
        boolean gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        String provider = LocationManager.GPS_PROVIDER;
        if (network_enabled)
            provider = LocationManager.NETWORK_PROVIDER;

        return mLocationManager.getLastKnownLocation(provider);
    }

    public void sendResult() {
        intent.putExtra("time", new Date().toLocaleString());
        sendBroadcast(intent);
    }

    private void alert(Location location){
        if (NotificationDao.LIST == null) {
            return;
        }
        for(NotificationDao item : NotificationDao.LIST) {
            Location l2 = new Location("");
            l2.setLatitude(item.getLat());
            l2.setLongitude(item.getLng());
            if (location.distanceTo(l2) <= ALERT_RANGE) {
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                for(int i = 0; i < 3; i++){
                    v.vibrate(500);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
