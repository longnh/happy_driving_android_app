package dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LongNH on 8/1/15.
 */
public class TestDao {

    @SerializedName("ip")
    private String ip;

    @SerializedName("country_code")
    private String country_code;

    public TestDao(String ip, String country_code) {
        this.ip = ip;
        this.country_code = country_code;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}


