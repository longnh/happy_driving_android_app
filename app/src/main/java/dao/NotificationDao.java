package dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by LongNH on 8/1/15.
 */
public class NotificationDao {
    public static List<NotificationDao> LIST;
    private int id;
    private String name;
    private double lat;
    private double lng;
    private String description;
    private String formatted_address;
    private int gmap_id;
    private String report_type;
    private String created_at;
    private String updated_at;

    public NotificationDao(int id, String name, double lat, double lng, String description, String formatted_address, int gmap_id, String report_type, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.description = description;
        this.formatted_address = formatted_address;
        this.gmap_id = gmap_id;
        this.report_type = report_type;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public int getGmap_id() {
        return gmap_id;
    }

    public void setGmap_id(int gmap_id) {
        this.gmap_id = gmap_id;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
